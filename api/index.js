const express = require('express')
const bodyParser = require('body-parser')
const low = require('lowdb')
const FileAsync = require('lowdb/adapters/FileAsync')

// Create server
const app = express()
app.use(bodyParser.json())

const reloadDbEvery = process.env['RELOAD_DB_EVERY']

// Create database instance and start server
// const adapter = new FileAsync('../db/db.light.json')
const adapter = new FileAsync('../db/db.json')
low(adapter)
  .then(db => {
    // Routes
    // GET /stations/:id
    app.get('/stations', (req, res) => {
      const stations = db.get('stations')
        .filter(s => s.description.startsWith(req.query.q.toUpperCase()))
        .orderBy(['description'])
        .take(req.query.limit || 9999999999)
      res.send({hits: stations.size(), stations: stations})
    })

    if(reloadDbEvery){
      setInterval(() => db.read(), reloadDbEvery)
    }

    // Set db default values
    return db.defaults({ stations: [] }).write()
  })
  .then(() => {
    app.listen(3000, () => console.log('listening on port 3000'))
  })